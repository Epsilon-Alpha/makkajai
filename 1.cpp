#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Sales
{
    const int taxPercent = 10;   //Sales Tax %
    const int importPercent = 5; //Additional Import Tax %
    string filename;
    ifstream fin;
    vector<string> whitelist = {"book", "chocolate", "pills"};

public:
    Sales(string name)
    {
        this->filename = name;
        try
        {
            fin.open(name);
            if(fin.is_open())
                process();
        }
        catch(char const *s)
        {
            cerr << s << endl;
        }
    }

    void process()
    {
        string line;
        double totalTax = 0;
        double total = 0;
        while(fin.good())
        {
            getline(fin, line);
            int len = line.length();
            //Format: Number Item at Cost
            int quantity = 0;
            string item, costString;
            bool imported = false;
            try
            {
                if(!isdigit(line[0]))
                    throw "Invalid format";
                int i, idx = line.find_last_of("at ");

                //Fetch quantity
                for(i = 0; line[i] != ' '; i++)
                    quantity = quantity * 10 + (line[i] - '0');

                //Fetch item name
                for(i = i + 1; i < idx - 3; i++)
                    item += line[i];

                //Fetch cost
                costString = line.substr(idx + 1);
                double cost = stod(costString);

                bool taxExempt = false;

                //Convert string to lowercase for string matching
                string itemLower = item;
                transform(itemLower.begin(), itemLower.end(), itemLower.begin(), ::tolower);
                for(string s : whitelist)
                {
                    if(itemLower.find(s) != -1)
                    {
                        taxExempt = true;
                        break;
                    }
                }
                double currentTax = 0;
                if(!taxExempt)
                    currentTax += (taxPercent / 100.0 * cost);
                if(line.find("imported") != -1) //imported
                    currentTax += (importPercent / 100.0 * cost);
                currentTax = ceil(currentTax * 20) / 20.0;
                totalTax += currentTax;
                total += cost + currentTax;

                cout << fixed << setprecision(2) << quantity << " " << item << ": " << cost + currentTax << endl;
            }
            catch(string msg)
            {
                cerr << msg << endl;
            }
        }
        cout << fixed << setprecision(2) << "Sales Tax: " << totalTax << endl;
        cout << fixed << setprecision(2) << "Total: " << total << endl;
    }
};

int main()
{
    Sales s1("q1_input1.txt");
    cout << endl;
    Sales s2("q1_input2.txt");
    cout << endl;
    Sales s3("q1_input3.txt");
    return 0;
}